public class MethAdd<AAA> {

    private int size = 0;
    private Object[] array = new Object[5];


    public void add(AAA count){
        if(size == array.length){
            grow(array.length + (array.length / 2));
        }
        array[size++] = count;
    }

    private void grow(int newLength) {
        Object[] newArray = new Object[newLength];
        System.arraycopy(array,0 , newArray, 0, size);
        array = newArray;
    }

    public int size() {
        return  size;
    }

}